import requests
import sys

CARD_URL = "https://db.ygoprodeck.com/api/v7/cardinfo.php/?"
SET_URL = "https://db.ygoprodeck.com/api/v7/cardsets.php?"

def main():
    set_response = requests.get(SET_URL)
    valid_sets = set()
    for card_set in set_response.json():
        try:
            if card_set["tcg_date"] <= "2008-03-26":
                valid_sets.add(card_set["set_name"])
        except KeyError:
            # no date means not tcg
            pass

    
    card_response = requests.get(CARD_URL)
    banlist = []
    for card in card_response.json()["data"]:
        # if none of the sets are valid
        try:
            if not any(card_set["set_name"] in valid_sets for card_set in card["card_sets"]):
                banlist.append(card)
        except KeyError:
            # the card is part of no known tcg sets
            banlist.append(card)
    return
    
    with open("CoG2021.lflist.conf", 'wb') as banfile:
        for card in banlist:
            card_id = card["id"]
            card_name = card["name"]
            line = u'{card_id} 0 --{card_name}\n'.format(card_id=card_id, card_name=card_name).encode(sys.stdout.encoding, errors = 'replace')
            banfile.write(line)


main()