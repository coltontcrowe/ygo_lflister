This project exists to create lflists (banlists) for Yugioh at a provided time.  These lists are intended to work with programs such as EDO Pro.

# Functionality
- Add all cards that did not exist at the time to the forbidden list
- Add the banned cards from the provided date to the forbidden list
- Add the limited cards (1 copy of these cards allowed) from the provided date to the limited list
- Add the semi-limited cards (2 copies of these cards allowed) from the provided date to the semi-limited list

# Limitations
- Reliant on external websites for functionality.  If these websites go down, the project will no longer work.
- Hard coded date right now.  This will be updated in the future.
- Command line only at the present
- Only focuses on the TCG format
- Does not handle unofficial / anime-only cards
- Uses the most recent errata of cards rather than the version that existed at the time

# TODO
- [ ] Set up repository and environment
- [ ] Refactor existing code
- [ ] Add ban-lists from the time provided
- [ ] Add CLI
- [ ] Add GUI
